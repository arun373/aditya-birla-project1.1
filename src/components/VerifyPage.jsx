import React from 'react';

import AdityaLogo from './AdityaLogo'
import Card from "./CardComponent";
import QueryComponent from './QueryComponent';
import VerifyComponent from './VerifyComponent';
const VerifyPage = () => {

    return (
      <div style={{ backgroundColor: "rgb(247 247 247)"}}>
        <AdityaLogo />
          <Card />
          <QueryComponent />
         <VerifyComponent />
      </div>
    )
}

export default VerifyPage;