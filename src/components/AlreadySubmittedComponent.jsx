import React from 'react';
import { Link } from 'react-router-dom';


const AlreadySubmittedComponent = () =>{

    return(
        
     <Link to="/verify" style={{color: "#C7222A"}} className="d-flex justify-content-center">Already submited</Link>
    
    )
}

export default AlreadySubmittedComponent;