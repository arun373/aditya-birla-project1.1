import React from "react";

import AdityaLogo from './AdityaLogo'
import Card from "./CardComponent";
import QueryComponent from './QueryComponent';
import ClaimformComponent from "./ClaimformComponent";
import HealthCardComponent from "./HealthCardComponent";
import KycComponent from "./KycComponent";
import ImplantStikerComponent from "./ImplantStikerComponent";
import DocumentComponent from "./DocumentComponent";
import InstructionComponent from "./InstructionComponent";
import FooterButtonComponent from "./FooterButtonComponent";



const Home = () => {

  return (
  
        <div style={{ backgroundColor: "rgb(247 247 247)"}}>
          <AdityaLogo />
          <Card />
          <QueryComponent />
          <ClaimformComponent />
          <HealthCardComponent />
          <KycComponent />
          <ImplantStikerComponent />
          <DocumentComponent />
          <InstructionComponent />
          <FooterButtonComponent />
        </div>
  
  )
}

export default Home;
