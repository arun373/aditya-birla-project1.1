import React from 'react';
import AdityaLogo from './AdityaLogo';
import SubQueryComponent from './SubQueryComponent';

const FaqPage = () => {
    return (
     <div style={{ backgroundColor: "rgb(247 247 247)",  height: "100vh"}}>
          <AdityaLogo />
          <SubQueryComponent />
     </div>
    )
}
export default FaqPage;