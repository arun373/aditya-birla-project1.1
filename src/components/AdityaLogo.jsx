import React from 'react';
import logo from './../assets/aditya.jpeg'

const AdityaLogo = () => {
    return (
        <div>
        <img src={logo} alt="logo-img" width="100%"/>
    </div>
    )
}

export default AdityaLogo;