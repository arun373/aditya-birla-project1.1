import React from "react";
import ReactDOM from "react-dom";
import "./styles/global.css";
import Store from "./contexts/Store";
import { Route, HashRouter, Switch } from "react-router-dom";

import Home from "./components/Home";
import VerifyPage from './components/VerifyPage';
import FaqPage from "./components/FaqPage";

ReactDOM.render(
  <React.StrictMode>
    <HashRouter hashType="slash">
      <Switch>
        <Store>
          {/* <Route  exact path="/:callid?" component={Home} /> */}
          <Route  exact path="/" component={Home} />
          <Route path="/verify" component={VerifyPage} />
          <Route path="/query" component={FaqPage} />  
        </Store>
      </Switch>
    </HashRouter>
  </React.StrictMode>,

  document.getElementById("root")
);
