import axios from "axios";

export default function connector() {
    // Set config defaults when creating the instance
    const instance = axios.create({
      baseURL: "https://cloud-dev.ubona.com/visapp/",
      // baseURL: "https://kotakgeneral.ubona.com/visapp/",
      headers: {
        "x-client-id": "kotak_general",
        'Access-Control-Allow-Origin':"*"
      },
    });
    return instance;
  }