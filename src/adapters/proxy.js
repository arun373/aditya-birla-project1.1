import axios from 'axios'
const proxy = (method, endPoint, data={}) => {
    return axios.post('https://eiaxtt9p70.execute-api.ap-south-1.amazonaws.com/staging/proxy/', {
        method: method,
        //url: 'https://kotakgeneral.ubona.com/visapp/'+endPoint,
        url: 'https://cloud-dev.ubona.com/visapp/'+endPoint,
        headers: { 'x-client-id': 'kotak_general' },
        data,
    });
};

export default proxy;