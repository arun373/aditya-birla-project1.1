export const hangUpState = {callback:"Post event loading..."};

export const hangUpReducer = (state = hangUpEventState, action) => {
  switch (action.type) {
    case "HANGUP_SUCCESS":
      return {...state};
    case "HANGUP_FAILED":
        return  {error:"Something unexpected!!"};

    default:
      return state;
  }
};
