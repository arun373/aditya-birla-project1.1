export const playState = {callback:"Audio Loading..."};

export const playReducer = (state = initialState, action) => {
  switch (action.type) {
    case "PLAY_SUCCESS":
      return {callback:action.payload};
    case "PLAY_FAILED":
        return {error:"Something unexpected!!"};

    default:
      return state;
  }
};