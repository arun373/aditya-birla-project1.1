export const userState = {
  amount: "Loading...",
  customer_name: "Loading...",
  due_date: "Loading...",
  insurance_number: "Loading...",
  insurance_type: "Loading...",

  
};

export const userdataReducer = (state = userState, action) => {
  switch (action.type) {
    case "USERDATA_SUCCESS":
      return { ...action.payload };
    case "USERDATA_FAILED":
      return { error: "Something unexpected!!" };

    default:
      return state;
  }
};
