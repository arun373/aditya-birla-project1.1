import connector from '../adapters/xhr'
import proxy from '../adapters/proxy'
export const paymetGatewayCallback = (callid) => {
  let connect = connector();
  if (import.meta.env.MODE !== "development")
  return connect.post(callid + "/pay/v1");
else return proxy("POST", callid + "/pay/v1");


};
export default paymetGatewayCallback;