import connector from '../adapters/xhr'
import proxy from '../adapters/proxy'
export const play = (callid, name) => {
  let connect = connector();
  if (import.meta.env.MODE !== "development")
    return connect.get(callid + "/play/" + name);
  else return proxy("GET", callid + "/play/" + name);
};
export default play;