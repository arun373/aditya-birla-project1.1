import connector from '../adapters/xhr'
import proxy from '../adapters/proxy'
export const sendSMS = (callid,user_id) => {
  let connect = connector();
  let postdata = {
    "callId": callid,
    "templateType": "PAYMENT_SUCCESS"
};
  if (import.meta.env.MODE !== "development")
    return connect.post(user_id + "/sendsms/v1",postdata);
  else return proxy("POST", user_id + "/sendsms/v1",postdata);
};
export default sendSMS;