import connector from '../adapters/xhr'
import proxy from '../adapters/proxy'

const userData = (callid, fields) => {
  let connect = connector();
  if (import.meta.env.MODE !== "development")
    return connect.get(callid + "/getUserData/v1?fields=" + fields);
  else return proxy("GET", callid + "/getUserData/v1?fields=" + fields);
};
export default userData;
