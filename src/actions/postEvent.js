import connector from '../adapters/xhr'
import proxy from '../adapters/proxy'

export const postEvent = (callid, key, val) => {
  let connect = connector();
  let postdata = {
    "submit_event": key,
    "submit_value": val
};
  if (import.meta.env.MODE !== "development")
  return connect.post(callid + "/submit/v1",postdata);
else return proxy("POST", callid + "/submit/v1",postdata);
};
export default postEvent;