import connector from '../adapters/xhr'
import proxy from '../adapters/proxy'
export const paymentStatusCallback = (billid) => {
  let connect = connector();
  if (import.meta.env.MODE !== "development")
  return connect.get("pay_status/"+billid);
else return proxy("GET", "pay_status/"+billid);


};
export default paymentStatusCallback;