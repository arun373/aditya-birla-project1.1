import connector from '../adapters/xhr'
import proxy from '../adapters/proxy'
export const hangUp = (callid) => {
  let connect = connector();
  if (import.meta.env.MODE !== "development")
  return connect.get(callid + "/complete/hangup");
else return proxy("GET", callid + "/complete/hangup");
};
export default hangUp;