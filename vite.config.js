import { defineConfig } from "vite";
import reactRefresh from "@vitejs/plugin-react-refresh";

// https://vitejs.dev/config/
export default defineConfig({
  base:"/visapp_ui/kotakgeneral2/",
  // base:"/kotakgeneral/",
  define:{
    'process.env': {}
  },
  assetsInclude: [
    // images
    "png",
    "jpe?g",
    "gif",
    "svg",
  ],
  plugins: [reactRefresh()],
});
